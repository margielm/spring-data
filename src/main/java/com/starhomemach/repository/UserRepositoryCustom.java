package com.starhomemach.repository;

import com.starhomemach.mode.User;

/**
 * Created by mmar on 2/19/14.
 */
public interface UserRepositoryCustom {
    User find();
}
