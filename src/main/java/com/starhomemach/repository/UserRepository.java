package com.starhomemach.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.starhomemach.mode.User;

public interface UserRepository extends JpaRepository<User,Long>, UserRepositoryCustom{

    User findOne(Long id);

    User save(User user);

    List<User> findAll();

    User findByFirstName(String firstName);

    @Query(nativeQuery =true, value= "Select * FROM User WHERE firstname=:fn and lastname=:ln")
    User findUserBy(@Param("fn")String firstName, @Param("ln")String lastName);

    @Modifying
    @Query("Update User user SET user.firstName=?2 WHERE user = ?1")
    void updateFirstNameFor(User user, String newName);
}
