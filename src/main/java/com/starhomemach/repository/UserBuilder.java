package com.starhomemach.repository;

import com.starhomemach.mode.User;

/**
 * Created by mmar on 2/19/14.
 */
public class UserBuilder {
    private String firstName= "Michal";
    public static UserBuilder aUser() {
        return new UserBuilder();
    }

    public UserBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public User build() {
        return new User().firstName(firstName);
    }
}
