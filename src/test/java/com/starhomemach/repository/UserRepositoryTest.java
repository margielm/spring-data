package com.starhomemach.repository;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.starhomemach.H2Test;
import com.starhomemach.mode.User;

public class UserRepositoryTest extends H2Test {

    @Autowired
    private UserRepository repository;

    @PersistenceContext
//    @Autowired
    private EntityManager em;


    @Test
    public void should_return_null_if_user_doesnt_exist() {
        //given

        //when
        User user = repository.findOne(1L);

        //then
        assertThat(user).isNull();
    }

    @Test
    public void should_fetch_saved_user() {
        //given
        User user = save(new User());

        //when
        User fetchedUser = repository.findOne(user.getId());

        //then
        assertThat(fetchedUser).isEqualTo(user);
    }

    @Test
    public void should_fetch_all_users() {
        //given
        User user1 = save(new User());
        User user2 = save(new User());

        //when
        List<User> users = repository.findAll();

        //then
        assertThat(users).containsOnly(user1, user2);
    }
    @Test
    public void should_fetch_page_of_users() {
        //given
        save(new User().firstName("a"));
        save(new User().firstName("b"));
        User user = save(new User().firstName("c"));

        //when
        Page<User> page = repository.findAll(new PageRequest(1, 2, new Sort(Sort.Direction.ASC,"firstName")));

        //then
        assertThat(page.getContent()).containsOnly(user);
    }


    @Test
    public void should_fetch_user_by_first_name() {
        //given
        String firstName = "Starhome";
        User user = save(new User().firstName(firstName));
        save(new User().firstName("Michal"));

        //when
        User fetchedUser = repository.findByFirstName(firstName);

        //then
        assertThat(fetchedUser).isEqualTo(user);
    }
    @Test
    public void should_fetch_user_by_first_name_and_last_name() {
        //given
        String firstName = "Starhome";
        User user = save(new User().firstName(firstName).lastName("Mach"));
        save(new User().firstName(firstName).lastName(firstName));

        //when
        User fetchedUser = repository.findUserBy(firstName, "Mach");

        //then
        assertThat(fetchedUser).isEqualTo(user);
    }

    @Test
    public void should_update_first_name() {
        User user = save(new User().firstName("abc"));

        repository.updateFirstNameFor(user, "new");
        em.flush();


        assertThat(repository.findByFirstName("new")).isNotNull();

    }

    private User save(User user) {
        User savedUser = repository.save(user);
        em.flush();
        return savedUser;
    }
}
